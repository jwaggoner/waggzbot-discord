"use strict";
var accounts = require ('../config/accounts.js');
var weather = require ('./weather.js');
var config = require('../config/config.js');
var chat = require('./miscchatfunctions.js');
const Discord = require('discord.js');
var db = require('./db.js');
let mlb = require('./mlbstandings');
exports.client = new Discord.Client();

var client = exports.client;

client.login(accounts.discordToken);

client.on('ready', () => {
    console.log('I am ready!');
})
function joinPartChannel (message, joinpart, callback) {
    var channelname = message.content.substr(6);
    var user = message.author;
	if (message.client.channels.find("name", channelname.substring(1)) !== null) {
		channelname = message.guild.channels.find("name", channelname.substring(1)).id;
	}
	var found = false;
	config.discordchannels.forEach(function (channel) {
		if ('<#' + channel.id + '>' === channelname || channel.id === channelname) {
			found = true;
            if (joinpart === "join") {
                //add to role
                message.guild.member(user).addRole(message.guild.roles.find("name", channel.role))
                    .then(callback("You were added to the " + channelname + " channel"))
                    .catch(console.log);
            }
            else if (joinpart === "part") {
                //remove from role
                message.guild.member(user).removeRole(message.guild.roles.find("name", channel.role))
                    .then(callback("You were removed from the " + channelname + " channel"))
                    .catch(console.log);
            }
		}
	});
	if (found === false) {
		var response = "Sorry, that doesn't appear to be a valid channel name. Here is a list of public channels you can join:\n";
		config.discordchannels.forEach(function (channel) {
			if (client.guilds.find("id", accounts.discordserverid).channels.find("id", channel.id) !== null && channel.unlisted === false) {
				response = response + '#' + client.guilds.find("id", accounts.discordserverid).channels.find("id", channel.id).name + "\n";
			}
		});
		callback(response);
	}
}

client.on('message', function(message) {
	if (message.author.bot === true) {
		return;
	}
	if (message.content.toLowerCase() === "!test") {
		message.author.send("test received");
	}

	if (message.content.substr(0,5).toLowerCase() === "!join" || message.content.substr(0,5).toLowerCase() === "!part") {
		joinPartChannel(message, message.content.substr(1,4).toLowerCase(), function (response) {
			message.author.send(response);
		});
		return;
	}
	if (message.content.substr(0,2).toLowerCase() === "wx") {
		weather.getairportwx(message.content.substr(3,6), function (response) {
			message.reply(response);
		});
		return;
	}
        if (message.content.substr(0,7).toLowerCase() === "weather") {
                weather.getairportwx(message.content.substr(8,11), function (response) {
                        message.reply(response);
                });
                return;
        }
	if (message.content.substr(0,5).toLowerCase() === "metar") {
		weather.getmetar(message.content.substr(6,9), function (response) {
			message.reply(response);
		});
		return;
	}
    if (message.content.substr(0, 6).toLowerCase() === "uptime") {
        message.reply(new Date(client.uptime).toISOString().slice(11, -5));
        return;
    }
    if (message.content.substr(0, 4).toLowerCase() === "seen") {
        chat.seen(message.content.substr(5));
        return;
    }
    if (message.content.substr(0, 12).toLowerCase() === "mlbstandings") {
        mlb.getStandings(message.content.substr(13), function (response) {
        	message.reply(response);
		});
    }
    if (message.content.toLowerCase() === "mlb") {
        mlb.getScores(function (response) {
        	message.reply(response);
		});
    }
    if (message.content.substr(0, 12).toLowerCase() === "serverstatus") {
        let options = message.content.substr(13).split(" ");
        chat.gamequery(options[0], options[1], options[2], function (response) {
            message.reply(response);
        });
    }

	//if (message.author.bot === true || message.channel.type !== "text") {
	//	console.log("Private or Bot Message from " + message.author.username + ": " + message.cleanContent);
	//}
	//else {
	    console.log("#" + message.channel.name + ": @" + message.author.username + ": " + message.cleanContent);
        db.LogMessage(message.author.username, message.cleanContent, message.channel.name);
        var botmsg = chat.processMessage(message.cleanContent);
        if (botmsg) {
            message.channel.send(botmsg);
        }
	//}
});

client.on('presenceUpdate', function(olduser, newuser) {
    //console.log("presenceupdate");
    //console.log(olduser.user.name);
    //console.log("NOW:");
    //console.log(newuser.presence);
    var game = newuser.presence.game === null ? null : newuser.presence.game.name;
    db.LogPresence(newuser.id, newuser.presence.status, newuser.user.username, game);
});


client.on('error', (error) => {
    console.log(error);
})
process.on('unhandledRejection', console.error);

