"use strict";
var accounts = require ('../config/accounts.js');
var appid = accounts.openweathermapapikey;
const Feels = require('feels');
var parseString = require('xml2js').parseString;

var fs = require('fs');
var parse = require('csv-parse');
var request = require("request");

function toCelsius(f) {
    return (5/9) * (f-32);
}

function getCardinalDirection (bearing, style) {
	if (bearing === undefined) {
		return "undefined";
	}
	var bearingindex = (bearing > 348.75 ? 0 : bearing);
	var index = Math.round(bearingindex / 22.5);

	var cardinals = [
		{full: 'north', abbr: 'N'},
		{full: 'north-northeast', abbr: 'NNE'},
		{full: 'northeast', abbr: 'NE'},
		{full: 'east-norteast', abbr: 'ENE'},
		{full: 'east', abbr: 'E'},
		{full: 'east-southeast', abbr: 'ESE'},
		{full: 'southeast', abbr: 'SE'},
		{full: 'south-southeast', abbr: 'SSE'},
		{full: 'south', abbr: 'S'},
		{full: 'south-southwest', abbr: 'SSW'},
		{full: 'southwest', abbr: 'SW'},
		{full: 'west-southwest', abbr: 'WSW'},
		{full: 'west', abbr: 'W'},
		{full: 'west-northwest', abbr: 'WNW'},
		{full: 'northwest', abbr: 'NW'},
		{full: 'north-northwest', abbr: 'NNW'},
	];
	return cardinals[index][style];
}


exports.getairportwx = function (airportcode, callback) {
	var parser = parse({delimiter: ','}, function(err, data){
		var found = false;
	    data.forEach (function (entry) {
	    	if (entry[1].toLowerCase() == airportcode.toLowerCase() ) {
	    		var lat = entry[4].substr(0, entry[4].indexOf(".")+3);
	    		var lon = entry[5].substr(0, entry[5].indexOf(".")+3);
	    		var url = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&units=Imperial&APPID=" + appid;
	    		console.log(url);

	    		request(url, function(error, response, body) {
	    			var wxstring = "";
	    			var result = JSON.parse(body);
	    			if (result.cod == "200") {
		    			var time = new Date(result.dt * 1000);
						var feelslikeindex;
						const config = {
							temp: result.main.temp,
							humidity: result.main.humidity,
							speed: result.wind.speed,
							units: {
								temp: 'f',
								speed: 'mph'
							}
						};
						if (result.main.temp >= 80) {
							feelslikeindex = 'HI';
						}
						else if (result.main.temp <= 32 && result.wind.speed >= 3) {
							feelslikeindex = 'WCI';
						}
						else {
							feelslikeindex = false;
						}
						
		    			wxstring = "Weather for " + entry[3] + " as of " + time.toUTCString();
		    			wxstring = wxstring + ":\nConditions: " + result.weather[0].description;
		    			wxstring = wxstring + "\nTemperature: " + Math.round(result.main.temp) + '\u00B0' + "F (" + Math.round(toCelsius(result.main.temp)) + '\u00B0' + "C)";
		    			wxstring = wxstring + " with " + Math.round(result.wind.speed) + " MPH winds from the " + getCardinalDirection(result.wind.deg, "full") + " (" + Math.round(result.wind.deg) + '\u00B0' + ")";
		    			if (feelslikeindex !== false) {
							var feelsliketemp = Math.round(new Feels(config).like(feelslikeindex));
							wxstring = wxstring + "\nFeels like: " + feelsliketemp + '\u00B0' + "F (" + Math.round(toCelsius(feelsliketemp)) + '\u00B0' + "C) ";
						}
		    			wxstring = wxstring + "\nRelative Humidity: " + result.main.humidity + "%,";
		    			wxstring = wxstring + " Pressure: " + result.main.pressure + " hPa";
		    			wxstring = wxstring + "\nCloud coverage: " + result.clouds.all + "%";
						
		    		}
		    		else {
		    			wxstring = result.message;
		    		}
	    			callback(wxstring);
	    		});
	    		found = true;
	    	}

	    });
	   	if (found === false) {
	  		callback("I could not locate airport code '" + airportcode + "'' in my files. ");
	  	}
	});

	fs.createReadStream('files/airports.csv').pipe(parser);
};

exports.getmetar = function (airportcode, callback) {
	var url = "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&hoursBeforeNow=3&mostRecent=true&stationString=" + airportcode;
	request(url, function(error, response, body) {
		parseString(body, function (err, result) {
			callback(result.response.data[0].METAR[0].raw_text[0]);
		});
	});
}
