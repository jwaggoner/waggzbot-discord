"use strict";

var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('files/waggzbot.db');


exports.LogMessage = function (user, msg, channel) {
    //console.log("insert message log");
    var stmt = db.prepare("INSERT INTO message_log (user, content, channel) VALUES (?, ?, ?)");

    stmt.run(user, msg, channel);

    stmt.finalize();
};

exports.LogPresence = function (userid, status, name, game) {

    //console.log("insert message log");
    var stmt = db.prepare("REPLACE INTO user_status (status, name, game, userid) VALUES (?, ?, ?, ?)");

    stmt.run(status, name, game, userid);

    stmt.finalize();
};

exports.lastPresence = function (value) {

    var stmt = db.prepare ("SELECT * FROM user_status WHERE name = ? OR userid = ? ");

    stmt.run(value, value);
    stmt.all(function (error, rows) {
        //console.log(rows);
       return rows;
    });
    stmt.finalize();

};

function closeDb() {
    console.log("closeDb");
    db.close();
}