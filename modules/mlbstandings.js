"use strict";
let request = require('request');
let _ = require('lodash');


let standings = '';
let scores = '';

function updateScores() {
    return new Promise(function (resolve, reject) {
        let recheck = scores.last_update + 30000;
        let now = Date.now();
        var tzoffset = (new Date()).getTimezoneOffset() * 60000;
        let today = (new Date(Date.now() - tzoffset));
        let tomorrow = new Date(today);
        tomorrow.setDate(today.getDate() + 1);
        today = today.toISOString().substring(0, 10);
        tomorrow = tomorrow.toISOString().substring(0, 10);

        if (typeof scores.last_update === 'undefined' || recheck < Date.now()) {
            let options = {
                url: 'https://www.mysportsfeeds.com/api/season/39/game/list?startdate=' + today + '&enddate=' + tomorrow,
                headers: {
                    'User-Agent': 'waggzbot (https://bitbucket.org/jwaggoner/waggzbot-discord)'
                },
                timeout: 10000
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                    return;
                }
                //console.log('error:', error); // Print the error if one occurred
                //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                //console.log('body:', body); // Print the HTML for the Google homepage.
                if (response.statusCode === 200) {
                    scores = JSON.parse(body);
                    scores.last_update = Date.now();
                    resolve("OK");
                }
                else {
                    reject("Error updating scores - " + response && response.statusCode + " Error: " + error);
                }

            });
        }
        else {
            resolve("OK");
        }
    });
}

function updateStandings() {
    return new Promise(function (resolve, reject) {
        let recheck = new Date(standings.standings_date);
        recheck.setHours(recheck.getHours() + 1);
        if (typeof standings.standings_date === 'undefined' || recheck < Date.now()) {
            let options = {
                url: 'https://erikberg.com/mlb/standings.json',
                headers: {
                    'User-Agent': 'waggzbot (https://bitbucket.org/jwaggoner/waggzbot-discord)'
                }
            };

            request(options, function (error, response, body) {
                console.log('error:', error); // Print the error if one occurred
                //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                //console.log('body:', body); // Print the HTML for the Google homepage.
                if (response.statusCode === 200) {
                    standings = JSON.parse(body);
                    resolve("OK");
                }
                else {
                    reject("Error updating standings - " + response && response.statusCode + " Error: " + error);
                }

            });
        }
        else {
            resolve("OK");
        }
    });
}


exports.getStandings = function (teamid, callback) {
    updateStandings().then(function () {
        let team = _.find(standings['standing'], function (o) {
            return o.team_id.search(teamid.toLowerCase()) !== -1;
        });
        let response;
        if (typeof team !== 'undefined') {
            response = `The ${team.first_name} ${team.last_name} are ranked ${team.ordinal_rank} in the ${team.conference}-${team.division}.
                  They are ${team.games_back} games behind and are ${team.last_five} in their last five games.
                  Their total win percentage is ${team.win_percentage} and they are ${team.conference_won}-${team.conference_lost} in their conference.`;
        }
        else {
            response = "Sorry, that team id wasn't found!";
        }
        callback(response);
    });
};

exports.getScores = function (callback) {

    updateScores().then(function () {
        let response = '\n';
        scores.forEach(function (item) {
            let awayScore;
            let homeScore;
            switch (item.gameDTO.status) {
                case 'IN_PROGRESS':
                    awayScore = item.gameDTO.awayScore !== null ? item.gameDTO.awayScore : item.gameUpdateDTO.awayScore;
                    homeScore = item.gameDTO.homeScore !== null ? item.gameDTO.homeScore : item.gameUpdateDTO.homeScore;
                    let inning = item.gameUpdateDTO.periodTimeLeft === 0 ? 'Top ' : 'Bottom ';
                    inning = inning + item.gameUpdateDTO.currentPeriod;
                    response = `${response}${item.gameDTO.awayTeamDTO.abbreviation} (${awayScore}) at ${item.gameDTO.homeTeamDTO.abbreviation} (${homeScore})  ${inning}\n`;
                    break;
                case 'UNPLAYED':
                    response = `${response}${item.gameDTO.awayTeamDTO.abbreviation} at ${item.gameDTO.homeTeamDTO.abbreviation}   ${item.gameDTO.playDateTime.substr(11)} ET\n`;
                    break;
                case 'PENDING_LIVE_REVIEW':
                case 'COMPLETED':
                    awayScore = item.gameDTO.awayScore !== null ? item.gameDTO.awayScore : item.gameUpdateDTO.awayScore;
                    homeScore = item.gameDTO.homeScore !== null ? item.gameDTO.homeScore : item.gameUpdateDTO.homeScore;
                    response = `${response}${item.gameDTO.awayTeamDTO.abbreviation} (${awayScore}) at ${item.gameDTO.homeTeamDTO.abbreviation} (${homeScore})  Final\n`;
                    break;
                default:
                    console.log('unhandled game status: ' + item.gameDTO.status);
                    break;
            }
        });
        callback(response);

    }).catch(function (reject) {
        callback(`I'm unable to get updated scores right now, there might be a problem with the data source. Please try again later. (${reject})`);
    });
};

