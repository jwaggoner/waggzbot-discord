"use strict";
var config = require ('../config/accounts.js');
const express = require('express');
const bodyParser = require('body-parser');
const striptags = require('striptags');

const app = express();
const port = 3000;

const DiscordWebhook = require("discord-webhooks");

let myWebhook = new DiscordWebhook(config.discordwebhook);

app.use(bodyParser.json());

/*myWebhook.on("ready", () => {
    myWebhook.execute({
        content:"Hello from a webhook",
        username:"Mr Webhook",
       //avatar_url:"https://example.com/image.png"
    });
});
*/
myWebhook.on("error", (error) => {
    console.warn(error);
});

function bitbucketHookContent (name, repo, title, html, action, href) {
    let content = '';
    switch (action) {
        case 'issue:comment_created':
            content = ' has added a new comment to an issue on ';
            break;
        case 'issue:updated':
            content = ' has updated an issue on ';
            break;
        case 'issue:created':
            content = ' has created an issue on ';
            break;
        default:
            return 'Incoming webhook of unknown type: ' + action;
    }
    content = name + content + '***' + repo + '***\n\n' + '**Title: **' + title + '\n**Content:**```' + striptags(html, '<a>') + '```\n**Link to issue: **' + href;
    return content;
}
app.post(config.webhooklisten, (req, res) => {
    let html = (typeof(req.body.comment) !== 'undefined' && typeof(req.body.changes) === 'undefined') ? req.body.comment.content.html : req.body.issue.content.html;
    let content = bitbucketHookContent(req.body.actor.display_name, req.body.repository.name, req.body.issue.title, html, req.headers['x-event-key'], req.body.issue.links.html.href);
    res.send('OK');
    myWebhook.execute({
        content: content,
        username:"Bitbucket Webhook",
        //avatar_url:"https://example.com/image.png"
    });
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});
