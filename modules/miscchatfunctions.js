"use strict";
let db = require('./db.js');
let Promise = require('promise');

exports.ACMECON = function () {
    /*
    var startDate = new Date(Date.now());
    var endDate = new Date ('2016', '07', '26', '15');
    var timeUntil = new Date(endDate - startDate);
    var milliseconds = Math.abs(timeUntil);
    var seconds = Math.floor (milliseconds / 1000);
    var minutes = Math.floor (seconds / 60);
    var hours = Math.floor (minutes / 60);
    var days = Math.floor (hours / 24);
    */
    return "ACMECON 2017 TBA!";
};

exports.processMessage = function (text) {
    let botmsg = "";
    if (text.toLowerCase() === "acmecon") {
        botmsg += exports.ACMECON();
    }
    if (text.toLowerCase() === "discord") {
        botmsg += "https://discord.gg/njqWMqE";
    }
    if (text.toLowerCase() === "gadgetcam" || text.toLowerCase() === "gadget cam") {
        botmsg += "https://www.gadget.webcam";
    }
    if (text.toLowerCase() === "dextercam" || text.toLowerCase() === "dexter cam" ||
        text.toLowerCase() === "mazarcam" || text.toLowerCase() === "mazar cam" ||
        text.toLowerCase() === "bobbycam" || text.toLowerCase() === "bobby cam" ||
        text.toLowerCase() === "carlingcam" || text.toLowerCase() === "carling cam") {
        botmsg += "http://www.seananigans.net/";
    }
    return botmsg;
};

exports.seen = function (name) {
    //console.log(name);
    let value = "";
    if (name.substr(0, 2) === "<@") {
        value = name.slice(2, -1);
    }
    else {
        value = name;
    }
    //console.log("result: ");
    let result = new Promise(function (resolve, reject) {
        db.lastPresence(value);
    });
    result.done(function () {
        console.log(result);
        return result;
    });
};

exports.gamequery = function (game, host, port, callback) {
    let Gamedig = require('gamedig');
    Gamedig.query({
        type: game,
        host: host,
        port: Number(port)
    }).then((state) => {
        console.log(state);
        let players = '';
        state.players.forEach(function (ele, index) {
            players = `${players}\n${ele.name} ${ele.frags}`;
        });
        callback(`${state.name}:\nMap: ${state.map}\nPlayers: ${state.players.length} of ${state.maxplayers}${players}`);
    }).catch((error) => {
        callback("Unable to get server info");
    });

};
